﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule11
{
    public enum Vacancies
    {
        Manager,
        Boss,
        Clerk,
        Salesman
    }

    public struct Employee
    {
        public string Name { private set; get; }
        public Vacancies vacancy;
        public int Salary { private set; get; }
        public DateTime date;

    }
}
